#Laboratory 4

### Jacob Hanko

### Laboratory 4 Write-Up

### Due: October 8th, 2015 8:00 AM

### This work is mine unless otherwise cited.

## Question 2:

 0 bipush 10 - Pushes the constant '10' onto the stack

 2 istore_1 - Pops the '10' off the stack and stores it in the variable 'i'

 3 bipush 20 - Pushes the constant '20' onto the stack

 5 istore_2 - Pops the '20' off the stack and stores it in the variable 'j'

 6 iconst_0 - Pushes the constant '0' onto the stack

 7 istore_3 - Pops the '0' off the stack and stores it in the variable 'k'

 8 iload_1 - Loads the variable 'i', pushes it onto the stack

 9 bipush 10 - Pushes the constant '10' onto the stack

11 if_icmple 23 (+12) - Pops two elements off of the stack and compares with '>' (i > 10)... if true, go to line 23

14 iload_2 - Loads the variable 'j', pushes it onto the stack

15 bipush 20 - Pushes the constant '20' onto the stack

17 if_icmpne 23 (+6) - Pops two constants off of the stack and compares with '==' (j == 20)... if both this statement and bytecode line 11 are true, then the program will continue within the if statement (this is the short-circuit segment you mentioned in the handout)

20 bipush 100 - Pushes the constant '100' on the stack

22 istore_3 - Pops the '100' off of the stack and stores it in the variable 'k'

23 iload_3 - Loads the variable 'k', pushes it onto the stack (in this program, it will be 0)

24 ireturn - Return the value of 'k'



