// Jacob Hanko
// Lab 4 Part 1
// Creating a program that has same bytecode
// but uses a while loop instead of a for loop
// This work is mine unless otherwise cited.

public class Lab4WhileLoop {
  public void f() {
    int sum = 0;
    int i = 0;
    while (i < 10) {
      sum = sum + i;
      i++;
    }
  }
}
