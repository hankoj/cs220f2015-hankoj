// Jacob Hanko
// Laboratory 4 - Installment 2
// Added the recursive function for recur
// This work is mine unless otherwise cited.

public class Lab4Part4b {
  public static void main(String[] args) {
    for (int i = 0; i <= 5; i++) {
      for (int j = 0; j <= 5; j++) {
        System.out.println("recur("+i+","+j+") = "+recur(i,j));
      }
    }
  }

  public static int recur(int i, int j) {

    /* replace this comment with your code */

	// if i ≤ 0 or j ≤ 0, recur(i, j) = 0
    if (i <= 0 || j <= 0) {
    	return 0;
	}

	// if i = j, recur(i, j) = i
	if (i == j) {
		return i;
	}

	// if i > j, recur(i, j) = j
	if (i > j) {
		return j;
	}

	// In all other cases, recur(i, j) = 2 · recur(i − 1, j) + recur(j − 1, i)
	else
	{
		return 2 * recur(i-1, j) + recur(j-1, i);
	}

  }
}
