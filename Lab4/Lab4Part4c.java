// Jacob Hanko
// Laboratory 4 - Installment 2
// Create your own recursive function... just no infinite recursion!
// This work is mine unless otherwise cited.

public class Lab4Part4c {

	public static void main (String[] args) {
		for (double d = -1.0; d <= 10.0; d++)
		{
			System.out.println("d ="+superPi(d));
		}

	}

	public static double superPi(double d) {
		// This recursive makes no mathematical sense, but it's what I came up with
		// Basically, if the value inputted is negative, return -3.14, or -1 superPi
		// If the value is greater than or equal to 0 AND even, return 3.14, or 1 superPi
		// If the value is anything else (basically when it is odd) it multiplies 3.14 by the 
		// recursive method superPi(d-2.0)
		// Cool huh? 

		if (d < 0) {
			return -3.14;
		}

		if (d >= 0 && d % 2 == 0)
		{
			return 3.14;
		}

		else
		{
			return 3.14 * superPi(d - 2.0);
		}

	}

}
