// Jacob Hanko
// Laboratory 4 - Installment 2
// Added the recursive function needed for pow2
// This work is mine unless otherwise cited.

public class Lab4Part4a {
  public static void main(String[] args) {
    for (int i = 0; i <= 20; i++) {
      System.out.println("pow2("+i+") = "+pow2(i));
    }
  }

  public static int pow2(int n) {

    /* replace this comment with your code */
     
    // If n<=0, then pow2(n) = 1  
    if (n <= 0) {
    	return 1;
	}

	// Otherwise, pow2(n) = 2 * pow2(n-1)
	else {
		return 2 * pow2(n-1);
	}


  }
}
