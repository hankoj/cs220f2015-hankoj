# Jacob Hanko
# Laboratory 5
# Introduction to Lists in Python
# This work is mine unless otherwise cited.

def countFunction(a,i):
    # This function counts how many times 'i' is in list 'a'
    # To run, create a list in python
    # Ex: a = [1,2,4,3,2,4,5,3,2,2,3,2,2]
    count = 0
    for j in a:
        if j == i:
            count+=1
            
    return count

def commonFunction(a,b):
    # This function takes two lists and returns another list
    # Containing all elements that are in both lists
    # a = [3,2,5,4,6,7,4] 
    # b = [7,2,8,9,4]
    # will output [2, 4, 7, 4]
    common = []
    for i in a:
        for j in b:
            if i == j:
                common.append(i)

    return common

def uncommonFunction(a,b):
    # This function takes two lists and returns a new list
    # containing all elements that in one or the other list but NOT both
    # a = [3,2,5,4,6,7,4] 
    # b = [7,2,8,9,4]
    # will output [3, 5, 6, 8, 9]
    # currently outputting [3, 2, 5, 4, 6, 7, 8, 9] ... logic seems sound though
 
    uncommon = []
    common = []
    for i in a:
        for j in b:
            if i != j:
                if i not in uncommon:
                    uncommon.append(i)
            else:
                common.append(i)

    for i in b:
        for j in a:
            if i != j:
                if i not in uncommon:
                    uncommon.append(i)
            else:
                common.append(i)



    return uncommon

def shiftFunction(a,k):
    # This function takes a list 'a' and an integer 'k'
    # and rotates the list 'k' positions to the left
    # If 'k' is negative, rotate to the right by -k positions

    if k >= 0:
        newList = a[k:len(a)] + a[0:k]

    if k < 0:
        newList = a[len(a)+k:len(a)] + a[0:len(a)+k]

    return newList

