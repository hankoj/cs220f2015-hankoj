# 5 Things Java People Should Know About C++

## Jacob Hanko

### 1.

+ Make sure you include 'using namespace std;' at the beginning of your program.

### 2.

+ System.out.println is now cout << someting << endl;

+ On the contrary, input is now cin >>;

### 3.

+ The 'public static void main...' is out the door.  You need 'int main()' instead.

### 4.

+ Calls to methods are exactly the same, so don't worry about that at all.

+ pirate.getName(); for example


### 5.

+ To compile, g++ name.cpp.

+ To run, ./a.out, or whatever you named your output file.
