// Laboratory 8
// Jacob Hanko
// C++ was my chosen language because...
// I worked with C# over the summer
// Python we reviewed recently
// And the last time I used C++ was 10th grade
//
// This work is mine unless cited otherwise.

#include <string>
#include <iostream>

using namespace std;

// Gator class
class Gator {
	public:
		// Constructor that defaults color to green
		Gator(string n)
		{
			name = n;
			color = "green";
		}

		// Constructor that asks for both
		Gator(string n, string c)
		{
			name = n;
			color = c;
		}

		string getName() { return name; } // returns name
		string getColor() { return color; } // returns color
		
		// sets the color to new color
		string setColor(string nc)
		{
			color = nc;
		}

		// hardcode changing of color
		string setColorHard() { color = "emerald"; }
	private:
		string name;
		string color;
};

// 2nd class, I'm assuming it can be the main one?
// If not... i couldn't work out how to alter things within a different class
int main() {

	cout << "Welcome to My Program" << endl;

	// make the two gators...
	// one defaulting the color, the other... not
	Gator gator1("Jeffrey");
	Gator gator2("Snuffaluffagus", "rainbow");

	// output stuff about #1
	cout << "Welcome " << gator1.getName() << " to the program." << endl;
	cout << gator1.getName() << " is " << gator1.getColor() << endl;

	// output stuff about #2
	cout << "Welcome " << gator2.getName() << " to the program." << endl;
	cout << gator2.getName() << " is " << gator2.getColor() << endl;

	// change the color of one gator
	// segmentation fault??? not sure what that means
	gator1.setColor("ruby");

	// output new info
	cout << gator1.getName() << " is now " << gator1.getColor() << endl;

}
