# Final Project Progress Report

## Fall 2015

## Jacob Hanko

### Clojure

Boy oh boy was Clojure fun to set up.  Colton told me to look in Leiningen, basically a packaged version of Clojure that made creating projects and other things like that hassle free.  That wasn't the easiest thing in the world to set up, and it took some help from Colton.  Once I got Clojure all nice and set up, I started to look up what the heck it was.  I learned a whole bunch, including its a functional programming language and that it doesn't force your program to be referentially transparent.  There's plenty of other things I learned, but they're all going to be included in our paper and I'm pretty sure Colton recorded some of the things we found already.

Since Colton knew a little bit about Clojure already, he put me in charge of the basics, kind of like how you provide the basic functions via program examples.  I have no formally written programs, mostly because I have been messing around in the REPL.  The REPL, or "Read-Eval_Print-Loop", allows programmers to test their understanding of the language without making and compliling program files and what not.

The most basic functions can be done once in REPL mode (by typing lein repl), such as (+ 3 3) = 6, or (quot 4 2) = 2.  A little more complex is the Hello World that every programmer loves to look up first.  To do that in Clojure...

``
(def hello (fn [] "Hello World"))

(hello)
``

Calling (hello) outputs "Hello World".

As for the rest of the stuff I've worked on... It's pretty much just messing around trying to learn how to define functions and strings and lists and such.  Much more will be added to the paper as well as some formal programs created, as soon as I figure out how to do that as well.
