# Final Project Proposal

## Fall 2015

## Jacob Hanko

### Clojure

1. Name and Partner's Name

	+ Jacob Hanko and Colton McCurdy

2. Language you wish to study

	+ Clojure

3. Details on Language Availability

	+ I believe that Clojure is not on the Alden lab computers, unfortunately.  The jar file can be obtained from clojure.org and there is an interactive, browser-based REPL at tryclj.com.  I do know it runs on the JVM though, so there may be ways to run it in Alden.

4. Why you chose this Language

	+ I am not familiar with the LISP language, or any of its dialects.  Colton's cousin works in industry and told him to look into learning a functional programming language, and told him to look at Clojure.  The only time I have used Clojure, or even seen it, was in a Computer Science 280 lab that Colton and I worked on previously this semester.  We attempted to complete one assignment in Clojure.  I would really like to learn more about it, especially because it is a LISP dialect AND its a functional language.
