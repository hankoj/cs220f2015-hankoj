/*
 * Jacob Hanko
 *
 * Tests the SuperHero class
 *
 * This work is mine unless cited otherwise.
 *
 *
 */

using System;

class TestSuperHero {
	static void Main() {

		// Make the superheroes
		SuperHero superman = new SuperHero("Superman", 74, 1);
		SuperHero batman = new SuperHero("Batman", 42, 2);
		SuperHero flash = new SuperHero("Flash", 36, 3);
		SuperHero antman = new SuperHero("AntMan", 29, 4);

		// Printout info about the Superheroes
		Console.WriteLine(superman.Name+" is "+superman.Age+" years old, rank "+superman.Rank+", can beat "+batman.Name+", who is "+batman.Age+" years old and is ranked "+batman.Rank);

		Console.WriteLine(batman.Name+" is "+batman.Age+" years old, rank "+batman.Rank+", can beat "+flash.Name+", who is "+flash.Age+" years old and is ranked "+flash.Rank);

		Console.WriteLine(superman.Name+" is "+superman.Age+" years old, rank "+superman.Rank+", can beat "+antman.Name+", who is "+antman.Age+" years old and is ranked "+antman.Rank);

		Console.WriteLine(antman.Name+" cannot beat anyone listed... because even at age "+antman.Age+", he still loses at "+antman.Rank+" to "+superman.Name+", "+batman.Name+", and "+flash.Name+".");

	}
}
