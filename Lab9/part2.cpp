/*
 * Jacob Hanko
 * Laboratory 9
 * Part 2
 * Multiple Inheritance Example
 *
 * This work is mine unless otherwise cited.
 */

#include <iostream>
using namespace std;

// First Class
// Inlcludes two instance variables
// a constructor
// and two get methods
class Country {
  private:
     string name;
     int pop;

  public:
     // constructor:
     Country(string name, int pop) {
       this->name = name; this->pop = pop;
     }

     string getName() { return name;}

     int getPop() { return pop;}
};

// Second Class
// Inlcude two instance variables
// a constructor
// and two get methods
class Lang {
  private:
     string language;
     int totalLangs;

  public:
     // constructor:
     Lang(string language, int totalLangs) {
       this->language = language; this->totalLangs = totalLangs;
     }

     string getLanguage() { return language;}

     int getTotalLangs() { return totalLangs;}
};

// Class Mix inherits the variables and methods from both 1 and 2, and
// in addition has two of its own instance variables and methods:

class AB: public Country, public Lang { // Note syntax for subclasses
  private:
     string pres;
     int year;

  public:
     // constructor. Note how we call the "superclass" constructors
     // in the header:
     AB(string name, int pop, string language, int totalLangs, string pres, int year): Country(name,pop), Lang(language,totalLangs) {
       this->pres = pres; this->year = year;
     }

     string getPres() { return pres;}

     int getYear() { return year;}
};

int main() {
   AB x("Alleghenia", 2100, "Nerdlandic", 14, "Mullen", 1815);
   cout << x.getName() << " has a population of " << x.getPop() << " and a primary language of " << x.getLanguage() << " out of  "  << x.getTotalLangs() << " total Languages.  The president is " << x.getPres() << " and was founded in " << x.getYear() << endl;
}
