/*
 * Jacob Hanko
 * Laboratory 9
 *
 * Superhero problem
 *
 * This work is mine unless cited otherwise.
 *
 */

using System;

class SuperHero {
	private string name;
	private int age;
	private int rank;

	// Constructor
	public SuperHero (string name, int age, int rank) {
		this.name = name;
		this.age = age;
		this.rank = rank;
	}

	// name property with only read access
	public string Name {
		get { return name; }
	}

	// age property with read and write access
	public int Age {
		get { return age; }
		set { age = value; }
	}

	// rank property with read and write access
	public int Rank {
		get { return rank; }
		set { rank = value; }
	}
}

