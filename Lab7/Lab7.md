# Lab 7 Write Up Questions

## Jacob Hanko

### 1. Think of a concrete application for function parameters--in what real-life programming task might we find it helpful to pass a function into another function as a parameter?

+ I think a good, concrete example of this would be if a programmer was analyzing a large amount of data.  Maybe he/she needs to cross-reference two sets of unorganized data and compare them for similar trends.  You could have an algorithm (function) that takes in a sorting algorithm (function) of each set of unorganized data.  This would pass a function into another function as a parameter and complete what the programmer is looking for the method(s) to do.

### 3. Think of a concrete applicaion for lambda expressions--in what real-life programming task might we find it helpful to create such "anonymous" functions at execution time?

+ Over the summer, I worked as a software engineer intern at Erie Insurance.  I was exposed to lambda expressions in C# while working on a large project that was reworking their online quote application.  One example I remember, which is a very real-world application, was automobile insurance coverage.  We had to write (well they had already written them) anonymous functions that would take in the different variables for an insurance policy, but the variables were different in every case.  The individuals insurance score, vehincle type, make, model, etc... were all needed to be calculated at execution time.
