/*
 * Jacob Hanko
 * Lab7 --> C program part
 *
 * Create f1, f2, f3, and calc
 * Similarly to how you did fpar.c
 *
 * This work is mine unless otherwise cited.
 *
 */

#include <stdio.h>
#include <math.h>

// f1
// Return half of the x value
int f1(double x)
{
	return (.5 * x);
}

// f2
// Returns the sqrt of x value
// *make sure to use the -lm flag to compile*
int f2(double x)
{
	return (sqrt((int)x));
}

// f3
// Makes the number negative
int f3(double x)
{
	return (-x);
}


// calc
// finds the product of opposite x * half of x * the sqrt of x
double calc(int (*f)(double), int (*g)(double), int (*h)(double), double x)
{
	return (f(x) * g(x) * h(x));
}


// main method
int main() 
{
	printf("Calculate using x = 4.0 -> %f\n",calc(f1,f2,f3,4.0));
	printf("Calculate using x = 25.5 -> %f\n",calc(f1,f2,f3,25.5));
	printf("Calculate using x = 125.0 -> %f\n",calc(f1,f2,f3,125.0));

}

