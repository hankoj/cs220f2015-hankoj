// Laboratory 7
// Part 2, Question 4
// Java Lambda Expressions
// Jacob Hanko
// This work is mine unless cited otherwise.

import java.util.*;
import java.util.function.*;

public class Lab7j {

	public static void main(String[] args) {

		// Run through 10 i values and apply the example lambda expression
		for (int i = 0; i < 10; i++)
			{
				int z = example(i,(Integer x)->(x*10)+(x*2));
				System.out.println("i = "+i+" and z = "+z);
			}

	}

	public static Integer example(int x, Function<Integer,Integer> f)
	{
		return f.apply(x);
	}

}
