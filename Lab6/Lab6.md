# Laboratory 6 - Haskell
# Jacob Hanko
# This work is mine unless otherwise cited.

1. Is Haskell a strongly-typed language?
	+ A strongly-typed language prevents you from applying an operation to data on which it is not appropriate, this definition was the one from the slides in class.  Upon first looking at the tutorial, I was leaning towards the answer being yes.  Once I consulted the __wiki.haskell.org__ site, it states that Haskell is, in fact, a strongly typed language.  The example they give in the wiki, is that it is impossible to convert a double to an int value on accident.

2. Can you explain "referencial transparency" in your own words or with an example?
	+ The textbook gives a very nice example of referencial transparency, it states that it basically means than if an expression has a certain value at one point in time, then it's guaranteed to have that value at any point in time.  In my own words, if I have some function that returns some value, lets say it returns x - 1, every time I pass in 5 to the function, I expect it to return the same thing, 4.

3. Haskell has multiple exponentiation operators.  Look them up, explain their differences, and give examples.
	+ Haskell has multiple exponentiation operators in order to cover all the exotic choices for basis and exponent.  The ^ exponent type covers repeated multiplication, ^- covers multiplication and division, ^/ covers rational numbers, and ^? covers exponential series and logs.  A nice chart for these are found on the Haskell wiki site.

4. Is there a clear operator precedence?
	+According to the Haskell website itself, there is a clear operator precedence.  It is as follows.
	+ .
	+ ^, ^^, **
	+ *, /, `quot`, `rem`, `div`, `mod`
	+ +, -
	+ ==, /=, <, <=, >=, > 
	+ &&
	+ ||
	+ >>, >>=  
	+ =<<
	+ $, $!, ‘seq‘

5. How do lists in Haskell differ from lists in Python?
	+ According to my own intuition, there are some functions that can be called in Haskell that cannot be called on a list in Python.  For example, you can use the minimum and maximum call in Haskell where in Python, there is no such thing.

6.  Haskell comments...
	+ The most interesting or unusual?
		+ The way you can add the ticks to some function call to change its fix, I think that is really interesting.
	+ The hardest to understand?
		+ The syntax was SO confusing to me for quite awhile.  I was trying to stuff logically how I would in other languages but it takes a second for your brain to think of it a different way.
	+ The most frustrating?
		+ Some information about Haskell is either hard to comprehend or hard to find.  I was struggling a tad through some of the questions.
	+ The most fun?
		+ I love learning new languages, so I really enjoyed doing this lab.
