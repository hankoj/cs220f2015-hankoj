--
-- Jacob Hanko
-- Laboratory 6
-- Due October 22, 2015 @ 8:00 am
-- This work is mine unless otherwise cited.
--
-- (a) This nontrivial and interesting function I chose to manipulate lists...
-- Cross-List Multiplier Maximizer! (clmax)
-- It takes in 2 lists as input, cross-multiplies them, and returns the max
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Defines the clmax function and takes in two lists...
-- one being n
-- the other being m 

clmax n m = 

-- multiplies n*m, binding n to n and m to m accordingly

	[n*m | n <- n, m <- m]

-- the final product will be a list of the cross-multiplications of the 
-- two lists
-- I was trying to add the ending of the clmax function so that it takes the maximum of the cross-product list... but I don't fully understand what is being returned so I can't add a "maximum ___" part at the end, if that's even how you do it
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-- (b) Do something interesting with strings...
-- so I made a function that switches around consonants and vowels
vowels = ['a', 'e', 'i', 'o', 'u']
-- helper list with a list of the vowels... minus "y"

switcheroo ls = 
				if (head ls) `elem` vowels then
					"z" ++ (ls)

				else if (last ls) `elem` vowels then
					(ls) ++ "y"

				else
					ls

-- note: Not working... not really sure what's going on because I tried to rework a Haskell one you showed in class but with my own twist					

-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- (c) My program calculates how many hours/days you spent in earth days, when you input how many hours you were on venus
--

hoursCalc c = c * 5832
	--putStrLn "hours on Earth"
